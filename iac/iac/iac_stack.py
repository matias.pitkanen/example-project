from aws_cdk import core as cdk

# For consistency with other languages, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.
from aws_cdk import core
import os

from aws_cdk import (
    core,
    aws_ec2 as ec2,
    aws_ecs as ecs,
    aws_iam as iam,
    aws_ecr as ecr,
    aws_elasticloadbalancingv2 as elbv2,
    aws_logs as logs,
)

class IacStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        vpc_id = "vpc-0105053b6465cdac0"
        vpc = ec2.Vpc.from_lookup(self, id="VPC", vpc_id=vpc_id)
        cluster = ecs.Cluster(self, "apprentice-8-cluster", vpc=vpc)
        #service = ecs.Ec2Service(self, "apprentice-8-service", cluster=cluster)

        cluster.add_default_cloud_map_namespace(name="apprentice-8")

        servicePrincipal = iam.ServicePrincipal('ecs-tasks.amazonaws.com')
        task_role = iam.Role(self, 'ecsTaskExecutionRole-apprentice8', assumed_by=servicePrincipal)
        policy = iam.ManagedPolicy.from_aws_managed_policy_name('service-role/AmazonECSTaskExecutionRolePolicy')
        task_role.add_managed_policy(policy)

        frontend_ecr_repo = ecr.Repository.from_repository_name(self, id="frontend-apprentice-8", repository_name="apprentice-8-frontend")
        backend_ecr_repo = ecr.Repository.from_repository_name(self, id="backend-apprentice-8", repository_name="apprentice-8-backend")

        group_backend = ec2.SecurityGroup(self, "backend-sg-apprentice-8", allow_all_outbound=True, security_group_name="BackendSG-apprentice-8", vpc=vpc)
        group_frontend = ec2.SecurityGroup(self, "frontend-sg-apprentice-8", allow_all_outbound=True, security_group_name="FrontendSG-apprentice-8", vpc=vpc)
        group_database = ec2.SecurityGroup(self, "database-sg-apprentice-8", allow_all_outbound=True, security_group_name="DatabaseSG-apprentice-8", vpc=vpc)

        group_backend.connections.allow_from_any_ipv4(ec2.Port.tcp(5000))
        group_frontend.connections.allow_from_any_ipv4(ec2.Port.tcp(8080))
        group_database.connections.allow_from_any_ipv4(ec2.Port.tcp(6379))

        frontendTaskDefinition = ecs.FargateTaskDefinition(self, "frontendTaskDefinition-apprentice-8", cpu=256, memory_limit_mib=512, task_role=task_role)
        backendTaskDefinition = ecs.FargateTaskDefinition(self, "backendTaskDefinition-apprentice-8", cpu=256, memory_limit_mib=512, task_role=task_role)
        databaseTaskDefinition = ecs.FargateTaskDefinition(self, "databaseTaskDefinition-apprentice-8", cpu=256, memory_limit_mib=512, task_role=task_role)

        feLogGroup = logs.LogGroup(self, "frontendLogGroup-apprentice-8", log_group_name="/ecs/frontendLogGroup-apprentice-8", removal_policy=core.RemovalPolicy.DESTROY)
        beLogGroup = logs.LogGroup(self, "backendLogGroup-apprentice-8", log_group_name="/ecs/backendLogGroup-apprentice-8", removal_policy=core.RemovalPolicy.DESTROY)
        dbLogGroup = logs.LogGroup(self, "databaseLogGroup-apprentice-8", log_group_name="/ecs/databaseLogGroup-apprentice-8", removal_policy=core.RemovalPolicy.DESTROY)

        frontLogDriver = ecs.AwsLogDriver(log_group=feLogGroup, stream_prefix="frontend")
        backLogDriver = ecs.AwsLogDriver(log_group=beLogGroup, stream_prefix="backend")
        dbLogDriver = ecs.AwsLogDriver(log_group=dbLogGroup, stream_prefix="database")

        frontContainer = frontendTaskDefinition.add_container(
            "frontend-apprentice-8",
            image=ecs.ContainerImage.from_ecr_repository(frontend_ecr_repo, tag=os.environ["CI_COMMIT_SHORT_SHA"]),
            environment={"COLORS_BACKEND_ADDRESS": "backend.apprentice-8:5000",},
            logging=frontLogDriver)
        frontContainer.add_port_mappings(ecs.PortMapping(container_port=8080))
        
        backContainer = backendTaskDefinition.add_container(
            "backend-apprentice-8",
            image=ecs.ContainerImage.from_ecr_repository(backend_ecr_repo, tag=os.environ["CI_COMMIT_SHORT_SHA"]),
            environment={"COLORS_DATABASE_ADDRESS": "database.apprentice-8:6379",},
            logging=backLogDriver)
        backContainer.add_port_mappings(ecs.PortMapping(container_port=5000))
        
        dbContainer = databaseTaskDefinition.add_container(
            "database-apprentice-8",
            image=ecs.ContainerImage.from_registry("redis:6.0-alpine"),
            logging=dbLogDriver)
        dbContainer.add_port_mappings(ecs.PortMapping(container_port=6379))

        frontFGDef = ecs.FargateService(
            self,
            "FrontendFargateService-apprentice-8",
            cluster=cluster,
            task_definition=frontendTaskDefinition,
            security_group=group_frontend,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="frontend"),
            service_name="frontend")

        backFGDef = ecs.FargateService(
            self,
            "BackendFargateService-apprentice-8",
            cluster=cluster,
            task_definition=backendTaskDefinition,
            security_group=group_backend,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="backend"),
            service_name="backend")

        DBFGDef = ecs.FargateService(
            self,
            "DatabaseFargateService-apprentice-8",
            cluster=cluster,
            task_definition=databaseTaskDefinition,
            security_group=group_database,
            desired_count=1,
            cloud_map_options=ecs.CloudMapOptions(name="database"),
            service_name="database")

        loadBalancer = elbv2.ApplicationLoadBalancer(self, "external-apprentice-8", vpc=vpc, internet_facing=True)
        LBListener = loadBalancer.add_listener("alb-listener-apprentice-8", port=80)
        loadBalanderHealth = elbv2.HealthCheck(path='/')
        LBListener.add_targets("tg-apprentice-8", port=80, health_check=loadBalanderHealth, targets=[frontFGDef])

        core.CfnOutput(self, 'ALB DNS: ', value=loadBalancer.load_balancer_dns_name)